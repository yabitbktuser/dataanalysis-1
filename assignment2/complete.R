complete <- function(directory, id = 1:332)
{
	source("getmonitor.R")
	v <- matrix(0, nrow=length(id), ncol=2)
	ValidData <- vector()
	nr <- 1
	for (i in id)
	{
		fdata <- getmonitor(i, directory)
		numvalid <- 0
		for (j in 1:nrow(fdata))
		{
			if ((! is.na(fdata[j,"nitrate"])) && (! is.na(fdata[j, "sulfate"])))
			{
				numvalid <- numvalid + 1
			}
		}

		v[nr,] <- c(i, numvalid)
		nr <- nr + 1
	}
	
	df <- data.frame(id = v[,1], nobs = v[,2])
	return(df)
}
