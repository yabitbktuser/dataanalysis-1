outcome <- read.csv("outcome-of-care-measures.csv", colClasses = "character")
#par(mfrow = c(3, 1))
par(mfrow = c(1, 3))
ha <- na.omit(as.numeric(outcome[,11]))
hf <- na.omit(as.numeric(outcome[,17]))
pn <- na.omit(as.numeric(outcome[,23]))
rha <- range(ha)
rha
rhf <- range(hf)
rhf
rpn <- range(pn)
rpn
hist(ha, main = "Heart Attack", xlab = "30-Day Death Rate")
hist(hf, main = "Heart Failure", xlab = "30-Day Death Rate")
hist(pn, main = "Pneumonia", xlab = "30-Day Death Rate")
